Wellbeing Support Resource campaign site
========================================

This is a small website built as a counter part with Early Recovery's PDF documents. 
It's part of a Concierge MVP and the site will likely be 'throw away' code in a few months time. 

## Getting started

## Requirements

1. Install [node.js](https://nodejs.org) v14.17.4 LTS (I recommend using [nvm](https://github.com/nvm-sh/nvm/blob/master/README.md) to install and manage different node.js versions)
2. Install [Yarn](https://classic.yarnpkg.com/en/docs/install/)
3. Clone this repo and run `npm install` or `yarn`
4. Run `yarn start` to spin up the development

## Folder structure

```
- config        # Webpack and environment configs
- dist          # production files, to be uploaded to [AWS S3 bucket](https://s3.console.aws.amazon.com/s3/buckets/wellbeingservice.com.au?region=ap-southeast-2&tab=objects)
- src
  - constants
  - data        # content lives here, such as site config, navigation, pages and content blocks
  - images
  - scss        # Sass stylesheets for global styling. Component-specific styling is done via Sass partials stored in the relative folder
  - state       # manageunidirectional states and actions with mithril/stream
  - view        # components and routes
  index.html    # the main index.html file
  index.js      # the main/root js file
  index.scss    # styles (TODO: componentised styling)
```

## Editing content

Content lives within the `data` object. There are three types of data organised in javascript objects:
- Page content: an array of content block IDs to render
- Content blocks: all the content blocks. I'm using the block paradigm because the same content can be re-used within different skins/themes
- Navigation items
- Configuration: e.g. default values like home routes, menu state, call-to-action content etc

## Components

Mithril.js components are declared as Javascript variables named with the camelcase notation. 
For example `NavItem`.

## Styling

This site is styled with [Bulma CSS](https://bulma.io/). 
At the moment I've loaded the entire Bulma style and override them in `index.scss`. 
Ideally, I'd like to componentise the styles and only load in Sass partials as needed.

## Hidden from the public

The MVP is meant to be accessed by invite and campaigns only.
Therefore, `robots.txt` and meta data have been set up to block search engines. 

## Todos

- [x] Get domain name
- [ ] Cross browser/device testing, especially IE11
- [x] Lay out draft content
- [ ] Skip to main content (a11y)
- [x] Small screen navigation
- [x] HTML content 
- [x] Two skins
- [x] Fancy up hero banners with images (waiting on images from Marketing)
- [x] Call-to-action on every page
- [ ] HTTPS
- [x] Default "/" to index.html
- [x] Introduce more interesting layout for the pages
- [ ] Show vendors who support this product
  - [ ] footer logos
  - [x] Visually differentiate text and text links
- [ ] Redirect non-www to www
- [x] Bi-directional binding with [`mithril/stream`](https://mithril.js.org/stream.html)
- [x] Tracking
  - [x] Install Matomo
  - [x] As a SPA, Ensure different routes are tracked by Matomo
  - [ ] Add event tracking for contact and external links

## Finessing and refactor

I'll do this if the MVP will live longer than 6 months.

- [ ] Refactor:
  - [x] Unidirectional states
  - [ ] Optimise page size: asynchronously request for content on different pages
  - [ ] Styling: I'm sorry, my Sass declarations are now very convoluted
  - [ ] Don't load Mithril globally
  - [ ] Replatform to [`nuxt.js`](https://nuxtjs.org)?
- [ ] Write TESTS!!!
- [ ] Optimise
- [ ] Styling finessing
  - [ ] Selected nav styling
  - [ ] favicon.ico
  - [x] scrollTop after route change
  - [x] Mobile menu styling
- [ ] Error catch-all page (e.g. 404)
- [ ] Automatically move robot.txt to dist (webpack)
- [ ] Fix dependency vulnerabilities

## Why `Mithril.js`?

It's built in [Mithril.js](https://mithril.js.org) for the following early constraints:
- when I was assigned this project, I didn't have sufficient permission to be able to set up a local development environment, which means:
  - No Git
  - No node.js
  - Can't set up SSH
- I'd like to have a templating system

In light of this, I've decided to use Mithril.js because it's lightweight 
and doesn't require a fully-fledged development setup to get started.

This does mean that I've set up the files in a specific way to strike a balance between code organisation and frontend performance.

Since I receiving my Macbook, I've been refactoring the code base to use [Webpack](https://webpack.js.org) and setting up a proper folder structure. 
It's no longer _a god index.html file to rule 'em all_ (css and Javascript all inlined in one index.html file. See [initial commit](https://bitbucket.org/SUO1/wsv-wellbeing-support/commits/efbbdcb5ad8162a9f6fda4b94ee668d48169c844)).

Had I received my Macbook earlier I would've went with `nuxt.js`/`VueJS` to align with our tech stack. That being said, it's been an interesting learning experience using a lightweight javascript framework.

## Need help?

Contact Oly [Oly_Su@worksafe.vic.gov.au](mailto:Oly_Su@worksafe.vic.gov.au)
