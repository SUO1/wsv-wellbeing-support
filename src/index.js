import 'core-js/stable';
import 'regenerator-runtime/runtime';
import Layout from './view/components/Layout';
import './index.scss';
import { pages } from './data';

if (module.hot) {
  module.hot.accept();
}

if (process.env.NODE_ENV !== 'production') {
  console.log('Looks like we are in development mode!');
}

console.log('Env var test ===>', process.env.BASE_URL, process.env.BASE_URL_EXPAND);
const $root = document.body.querySelector('#root');

m.route(
  $root,
  pages.home.route,
  Object.keys(pages)
    .reduce((a, pageID) => (
      Object.assign(a, { 
        [`${pages[pageID].route}`]: { view: () => m(Layout, { pageID, pages }) },
      })), {}),
);
