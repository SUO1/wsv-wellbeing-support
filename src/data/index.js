import config from './config';
import navigation from './navigation';
import contentBlocks from './contentBlocks';
import pages from './pages';

export { config, navigation, contentBlocks, pages };
export default { config, navigation, contentBlocks, pages };
