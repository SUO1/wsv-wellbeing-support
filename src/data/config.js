const defaults = {
  headingLevel: 2,
  isMenuActive: false,
  skin: 'retail',
};

const ece = {
  skin: 'ece',
  callToAction: {
    title: 'Reach out today',
    tagline: `
      Check-in with your mental health. 
      <strong>Free</strong> and confidential support is available <strong>for early childhood workers</strong>.
    `,
    primary: {
      data: 'worksafe@injurynet.com.au',
      label: 'Email',
      type: 'default',
    },
    secondary: {
      data: '1300945675',
      label: '1300 945 675',
    },
  },
};

const retail = {
  skin: 'retail',
  callToAction: {
    title: 'Reach out today',
    tagline: `
      Check-in with your mental health. 
      <strong>Free</strong> and <strong>confidential</strong> support is available <strong>for all retail workers</strong>
    `,
    primary: {
      data: 'support@transitioningwell.com.au',
      label: 'Request a call',
      type: 'request',
    },
    secondary: {
      data: '1300824808',
      label: '1300 824 808',
    },
  },
};

const config = {
  defaults,
  ece: {
    ...defaults, ...ece,
  },
  retail: {
    ...defaults, ...retail,
  },
};

export default config;
