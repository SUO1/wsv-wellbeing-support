const pages = {
  'home': {
    title: 'Don\'t let a little stress become a big problem.',
    label: 'home',
    route: '/',
    type: 'top-level',
    subtitle: 'Your guide to WorkSafe’s Wellbeing Support Service and navigating mental health issues at work.',
    contentBlockIDs: [
      'retail-home-what-is-the-wellbeing-support-service',
      'retail-home-supported-by-logo',
    ],
    children: [
      'retail-about',
      'retail-get-support',
      'retail-workplace-mental-injury',
    ],
    heroImage: {
      focusX: '50%',
      focusY: '50%',
      src: 'images/home.jpg',
    },
    callToAction: {
      primary: {
        type: 'request',
      },
    },
    skin: 'retail',
  },
  'retail-about': {
    title: 'About the Wellbeing Service',
    label: 'About the Wellbeing Service',
    route: '/about',
    type: 'default',
    contentBlockIDs: [
      'retail-about-am-i-eligible',
      'retail-about-how-will-it-help-me',
      'about-when-should-i-use-it',
      'retail-about-delivered-by-transitioningwell',
    ],
    parent: 'retail-home',
    skin: 'retail',
  },
  'retail-get-support': {
    title: 'Where to get support',
    label: 'Where to get support',
    route: '/get-support',
    type: 'default',
    subtitle: 'Call <a href="tel:1300824808">1300 824 808</a> or email <a href="mailto:support@transitioningwell.com.au">support@transitioningwell.com.au</a> to book an appointment or to ask for more information on the Wellbeing Support Service.',
    contentBlockIDs: [
      'get-support-where-else',
      'retail-get-support-gp',
      'get-support-contact-details',
    ],
    parent: 'retail-home',
    skin: 'retail',
    nextPage: 'retail-workplace-mental-injury',
  },
  'retail-workplace-mental-injury': {
    title: 'Workplace mental injury',
    label: 'Workplace mental injury',
    route: '/workplace-mental-injury',
    type: 'default',
    subtitle: 'Sometimes it’s hard to understand why you are feeling bad or why you are enjoying work or life less. Below are some of the reasons why you might be struggling, along with resources and suggestions for you to consider.',
    contentBlockIDs: [
      'workplace-mental-injury-intro',
      'workplace-mental-injury-what-is-mental-injury',
      'workplace-mental-injury-how-do-i-know-mental-injury',
      'workplace-mental-injury-checklist',
      'workplace-mental-injury-free-resources',
      'workplace-mental-injury-free-resources-list',
      'workplace-mental-injury-your-right',
      'workplace-mental-injury-compensation',
      'workplace-mental-injury-advisory',
      'workplace-mental-injury-contact',
    ],
    parent: 'retail-home',
    children: [
      'retail-work-related-stress',
      'retail-workplace-conflict-and-bullying',
    ],
    skin: 'retail',
    inPageNav: [
      {
        label: 'Workplace mental injury',
        route: '/workplace-mental-injury',
      },
      {
        label: 'Work-related stress',
        route: '/workplace-mental-injury/work-related-stress',
      },
      {
        label: 'Workplace conflict and bullying',
        route: '/workplace-mental-injury/workplace-conflict-and-bullying',
      },
    ],
  },
  'retail-work-related-stress': {
    title: 'Work-related stress',
    label: 'Work-related stress',
    route: '/workplace-mental-injury/work-related-stress',
    type: 'default',
    contentBlockIDs: [
      'workplace-mental-injury-work-related-stress',
      'retail-workplace-mental-injury-how-do-i-know',
      'workplace-mental-injury-what-can-i-do',
      'workplace-mental-injury-info',
    ],
    parent: 'retail-workplace-mental-injury',
    skin: 'retail',
    inPageNav: [
      {
        label: 'Workplace mental injury',
        route: '/workplace-mental-injury',
      },
      {
        label: 'Work-related stress',
        route: '/workplace-mental-injury/work-related-stress',
      },
      {
        label: 'Workplace conflict and bullying',
        route: '/workplace-mental-injury/workplace-conflict-and-bullying',
      },
    ],
  },
  'retail-workplace-conflict-and-bullying': {
    title: 'Workplace conflict and bullying',
    label: 'Workplace conflict and bullying',
    route: '/workplace-mental-injury/workplace-conflict-and-bullying',
    type: 'default',
    contentBlockIDs: [
      'workplace-mental-injury-conflict-bullying',
      'retail-workplace-mental-injury-how-do-i-know-conflict',
      'workplace-mental-injury-how-do-i-know-bullying',
      'workplace-mental-injury-not-bullying',
      'workplace-mental-injury-ongoing',
      'workplace-mental-injury-info-conflict-bullying',
    ],
    parent: 'retail-workplace-mental-injury',
    skin: 'retail',
    inPageNav: [
      {
        label: 'Workplace mental injury',
        route: '/workplace-mental-injury',
      },
      {
        label: 'Work-related stress',
        route: '/workplace-mental-injury/work-related-stress',
      },
      {
        label: 'Workplace conflict and bullying',
        route: '/workplace-mental-injury/workplace-conflict-and-bullying',
      },
    ],
    nextPage: 'retail-workplace-mental-injury',
  },
  'retail-information-pack': {
    title: 'Free counselling for your team',
    label: 'Free counselling for your team',
    route: '/free-counselling',
    type: 'landing-page',
    subtitle: 'WorkSafe’s Wellbeing Support Service is a free service available to all retail workers, to support their mental health and wellbeing.',
    heroImage: {
      focusX: '50%',
      focusY: '50%',
      src: 'images/home.jpg',
    },
    callToAction: {
      tagline: 'Help your team to feel better sooner. <strong>Free</strong> and <strong>confidential support</strong> is available for all retail workers',
      primary: {
        data: 'images/manager-information-pack.pdf',
        label: 'Download pack',
        type: 'download',
      },
    },
    contentBlockIDs: [
      'retail-landing-page-free-counselling',
      'retail-home-supported-by-logo',
    ],
    skin: 'retail',
  },
  'retail-assets': {
    title: 'How can you help your team',
    label: 'Free counselling assets',
    route: '/free-counselling-assets',
    type: 'default',
    subtitle: 'Let them know that this service is available to them, free of charge!',
    contentBlockIDs: [
      'retail-free-counselling-assets-poster',
      'retail-free-counselling-assets-social-media',
    ],
    skin: 'retail',
  },
  'retail-influencers': {
    title: 'Assets for social media influencers',
    label: 'Assets for social media influencers',
    route: '/free-counselling-social-influencers',
    type: 'default',
    contentBlockIDs: [
      'retail-free-counselling-assets-influencers',
    ],
    skin: 'retail',
  },
};

export default pages;
