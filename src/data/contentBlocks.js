const contentBlocks = {
  'home-what-is-the-wellbeing-support-service': {
    'content': `
      <h3>What is the Wellbeing Support Service?</h3>
      <p>The Service gives you access to 4 free telehealth appointments (that is, video-conference or telephone) with an allied health professional.</p>
      <p>The Wellbeing Support Service aims to prevent mental health injuries in the workplace by giving you a safe place to discuss and find solutions to any issues you’re having.</p>
      <p>The service is available to workers who do not have current access to an Employee Assistance Program.</p>
    `,
    'img': {
      'align': 'left',
      'alt': 'Lady using laptop',
      'src': 'images/home.jpg',
    },
  },
  'ece-home-supported-by-logo': {
    'content': `
      <div class="is-flex-tablet is-flex-direction-row is-align-items-center">
        <h3 class="my-4 mr-4">Supported by:</h3>
        <img alt="Logo: Early Learning Association Australia" src="images/logo-elaa.svg" style="max-width: 250px;" />
      </div>
    `,
  },
  'retail-home-supported-by-logo': {
    'content': `
      <div class="is-flex-tablet is-flex-direction-row is-align-items-center">
        <h3 class="my-4 mr-4">Supported by:</h3>
        <img alt="Logo: Australian Retailers Association Institute" src="images/logo-ara.png" style="max-width: 220px;" />
      </div>
    `,
  },
  'retail-home-what-is-the-wellbeing-support-service': {
    'content': `
      <h3>What is the Wellbeing Support Service?</h3>
      <p>The Service gives you access to 4 free telehealth appointments (that is, video-conference or telephone) with an experienced psychologist.</p>
      <p>The Wellbeing Support Service aims to prevent mental health injuries in the workplace by giving you a safe place to discuss and find solutions to any issues you’re having.</p>
      <p>The service is available to workers who do not have current access to an Employee Assistance Program.</p>
    `,
    'img': {
      'align': 'left',
      'alt': 'Man using laptop having an enojoyable video chat for work',
      'src': 'images/happy-worker-video-chat.jpg',
    },
  },
  'about-am-i-eligible': {
    'content': `
      <h3>Am I eligible for the Wellbeing Support Service?</h3>
      <p>You are eligible for the service if your workplace does not already offer free and confidential counselling, for example through an Employee Assistance Program (EAP).</p>
      <p>Everything you need to know about the Wellbeing Support Service:</p>
      <ul>
        <li>Available to workers who do not have access to an Employee Assistance Program</li>
        <li>Lets you access up to 4 free telehealth appointments with an allied health professional</li>
        <li>Confidential service – nothing you say will be shared with your employer or WorkSafe</li>
        <li>Appointments can be after hours and in languages other than English (subject to availability)</li>
        <li>Call <a href="tel:1300945675">1300 945 675</a> (Monday to Friday 8 am to 6 pm) Or, email: <a href="mailto:worksafe@injurynet.com.au">worksafe@injurynet.com.au</a></li>
      </ul>
    `,
    'img': {
      'align': 'right',
      'alt': 'Lady wearing headphones and using her smartphone',
      'src': 'images/lady-headphone.jpg',
    },
  },
  'retail-about-am-i-eligible': {
    'content': `
      <h3>Am I eligible for the Wellbeing Support Service?</h3>
      <p>You are eligible for the service if your workplace does not already offer free and confidential counselling, for example through an Employee Assistance Program (EAP).</p>
      <p>Everything you need to know about the Wellbeing Support Service:</p>
      <ul>
        <li>Available to workers who do not have access to an Employee Assistance Program</li>
        <li>Lets you access up to 4 free telehealth appointments with a registered psychologist</li>
        <li>Confidential service – nothing you say will be shared with your employer or WorkSafe</li>
        <li>Appointments can be after hours and in languages other than English (subject to availability)</li>
        <li>Call <a href="tel:1300824808">1300 824 808</a> (Monday to Friday 8 am to 6 pm) Or, email: <a href="mailto:support@transitioningwell.com.au">support@transitioningwell.com.au</a></li>
      </ul>
    `,
    'img': {
      'align': 'right',
      'alt': 'Lady wearing headphones and using her smartphone',
      'src': 'images/lady-headphone.jpg',
    },
  },
  'about-how-will-it-help-me': {
    'content': `
      <h3>How will the Wellbeing Support Service help me?</h3>
      <p>The Wellbeing Support Service gives you a confidential and safe place to discuss issues that are bothering you. Whether you are dealing with something at work or home, getting experienced support can help you cope better. Experienced allied health professionals can help you find ways to solve problems and improve your mood.</p>
      <p>You might want to use the Service if you are experiencing work-related stress, anxiety, depression, conflict, trauma or if you are being bullied or harassed.</p>
      <p>You might also want to use the Service if non-work related issues are making it harder to go to work, or to focus when you are there.</p>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration of a person waving friendly',
      'height': '250px',
      'src': 'images/illustration-waving-person.svg',
    },
  },
  'retail-about-how-will-it-help-me': {
    'content': `
      <h3>How will the Wellbeing Support Service help me?</h3>
      <p>The Wellbeing Support Service gives you a confidential and safe place to discuss issues that are bothering you. Whether you are dealing with something at work or home, getting experienced support can help you cope better. Psychologists can help you find ways to solve problems and improve your mood.</p>
      <p>You might want to use the Service if you are experiencing work-related stress, anxiety, depression, conflict, trauma or if you are being bullied or harassed.</p>
      <p>You might also want to use the Service if non-work related issues are making it harder to go to work, or to focus when you are there.</p>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration of a person waving friendly',
      'height': '250px',
      'src': 'images/illustration-waving-person-yellow.svg',
    },
  },
  'about-when-should-i-use-it': {
    'title': 'When should I use the Wellbeing Support Service?',
    'headingLevel': 3,
    'content': `
      <p>The earlier the better! Getting support earlier helps resolve the situation sooner, so you can start feeling better.</p>
      <p>If you think the confidential support of a professional might help, then the Wellbeing Support Service is for you.</p>
    `,
  },
  'about-delivered-by-injurynet': {
    'content': `
      <h3>The Wellbeing Support service is delivered by Injurynet</h3>
      <p>For more than two decades, Injurynet has delivered quality workplace medical services aimed at preventing and reducing the impact of injury and illness in workplaces and the community.</p>
      <p><a href="//www.injurynet.com.au" target="_blank" rel="external">www.injurynet.com.au</a></p>
    `,
    'img': {
      'align': 'right',
      'alt': 'Logo: Injurynet - A MAX SOLUTION Company',
      'src': 'images/injurynet-logo-with-tagline.png',
    },
    'hasBackgroundLight': true,
  },
  'retail-about-delivered-by-transitioningwell': {
    'content': `
      <h3>The Wellbeing Support service is delivered by Transitioning Well</h3>
      <p>Transitioning Well are a consultancy of registered psychologists who design and deliver unique services to support the mental health and wellbeing of individuals and the workplaces in which they work.</p>
      <p><a href="https://www.transitioningwell.com.au" target="_blank" rel="external">https://www.transitioningwell.com.au</a></p>
    `,
    'img': {
      'align': 'right',
      'alt': 'Logo: Transitioning Well',
      'height': '150px',
      'src': 'images/logo-transitioningwell.png',
    },
    'hasBackgroundLight': true,
  },
  'get-support-where-else': {
    'content': `
      <h3>Where else can I get support for my mental health?</h3>
      <p>If you don’t think the Wellbeing Support Service is for you, there are a lot of other options. You can, of course, seek support from many places at once.</p>
      <p>You might consider:</p>
      <ul>
        <li>Talking to a trusted friend or family member</li>
        <li>Making an appointment with your GP. They can refer you to a variety of different mental health professionals for further help</li>
        <li>Going directly to a mental health professional (if you have private health insurance, you might be eligible for a rebate to cover some of the costs)</li>
        <li>Calling a support helpline such as Lifeline (<a href="tel:131114">13 11 14</a>) or Beyond Blue (<a href="tel:1300224636">1300 22 4636</a>)</li>
        <li>Using mental health support websites such as Beyond Blue or Black Dog Institute</li>
        <li>Remember, if you are not sure who to contact, the Wellbeing Support Service is a good first step. They can offer support themselves, or help you figure out what option is best for you.</li>
      </ul>
    `,
    'img': {
      'align': 'right',
      'alt': 'Lady doing a video call with her friend on her laptop',
      'src': 'images/lady-video-call-with-friend.jpg',
    },
  },
  'get-support-gp': {
    'content': `
      <h3>How your GP can help</h3>
      <p>Your GP can help you with your mental health needs by:</p>
      <ul>
        <li>writing you a Mental Health Treatment Plan (this entitles you to a Medicare rebate for up to 20 sessions with a mental health professional)</li>
        <li>referring you to a psychologist, occupational therapist, counsellor or social worker</li>
        <li>referring you to a psychiatrist for more severe symptoms</li>
        <li>referring you to other appropriate health professionals, including mental health nurses, Aboriginal and Torres Strait Islander health workers and complementary health practitioners.</li>
      </ul>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration: a lady running and skipping forward joyfully on a light red background',
      'height': '250px',
      'src': 'images/illustration-lady-skipping.svg',
    },
  },
  'retail-get-support-gp': {
    'content': `
      <h3>How your GP can help</h3>
      <p>Your GP can help you with your mental health needs by:</p>
      <ul>
        <li>writing you a Mental Health Treatment Plan (this entitles you to a Medicare rebate for up to 20 sessions with a mental health professional)</li>
        <li>referring you to a psychologist, occupational therapist, counsellor or social worker</li>
        <li>referring you to a psychiatrist for more severe symptoms</li>
        <li>referring you to other appropriate health professionals, including mental health nurses, Aboriginal and Torres Strait Islander health workers and complementary health practitioners.</li>
      </ul>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration: a lady running and skipping forward joyfully on a light red background',
      'height': '250px',
      'src': 'images/illustration-lady-skipping-yellow.svg',
    },
  },
  'get-support-contact-details': {
    'content': `
      <p><strong>If you are distressed and need immediate emotional support, contact:</strong></p>
      <p>Lifeline: crisis support and suicide prevention service<br />
      <a href="tel:131114">13 11 14</a> | <a href="//www.lifeline.org.au" target="_blank" rel="external">www.lifeline.org.au</a></p>
      <p>Beyond Blue: counselling and support service<br />
      <a href="tel:1300224636">1300 224 636</a> | <a href="//www.beyondblue.org.au" target="_blank" rel="external">www.beyondblue.org.au</a></p>
    `,
    'content2': `
      <p><strong>If you have been physically or sexually assaulted, stalked, or had your property damaged, contact your local police station:</strong></p>
      <p>Find your local police station at <a href="//www.police.vic.gov.au/location" target="_blank" rel="external">www.police.vic.gov.au/location</a><br />
      Call <a href="tel:000">000</a> if it is an emergency.</p>
    `,
    'hasBackgroundLight': true,
  },
  'workplace-mental-injury-intro': {
    'content': `
      <ul class="my-0 is-size-4">
        <li><a href="/#!/ece/workplace-mental-injury/work-related-stress">Work-related stress</a></li>
        <li><a href="/#!/ece/workplace-mental-injury/workplace-conflict-and-bullying">Workplace conflict and bullying</a></li>
      </ul>
    `,
  },
  'retail-workplace-mental-injury-intro': {
    'content': `
      <ul class="my-0 is-size-4">
        <li><a href="/#!/retail/workplace-mental-injury/work-related-stress">Work-related stress</a></li>
        <li><a href="/#!/retail/workplace-mental-injury/workplace-conflict-and-bullying">Workplace conflict and bullying</a></li>
      </ul>
    `,
  },
  'workplace-mental-injury-what-is-mental-injury': {
    'content': `
      <h3>What is a workplace mental injury?</h3>
      <p>Workplace stress and conflict are not injuries or illnesses, but they can both lead to mental injury. Bullying can also lead to mental injury.</p>
      <p>Mental injuries include: </p>
      <ul>
        <li>depression </li>
        <li>anxiety </li>
        <li>burnout </li>
        <li>emotional distress </li>
        <li>self-harm or suicidal thoughts </li>
        <li>post-traumatic stress disorder (PTSD).</li>
      </ul>
      <p>Mental injuries are sometimes also known as mental health conditions or disorders. They are diagnosed by a medical practitioner. Symptoms can be short term or they can continue for many months or years and can make big changes to how a person feels, thinks, behaves and interacts with others.</p>
    `,
    'img': {
      'alt': 'Man doing video call on his laptop with colleagues',
      'src': 'images/man-video-call.jpg',
    },
  },
  'workplace-mental-injury-work-related-stress': {
    'content': `
      <h3>What is work related stress?</h3>
      <p>Work-related stress is when you feel overwhelmed by the demands of work or the workplace environment. It can make it harder to concentrate, to enjoy work or to feel safe at work. Ongoing stress can start affecting your physical and mental health.</p>
      <p>Sometimes stress goes away on its own, but getting support to reduce the stress is often a better approach.</p>
    `,
    'img': {
      'align': 'right',
      'alt': 'Happy lady working on her laptop and taking notes',
      'src': 'images/tile-news-newly-remote-workers_tcm7-270752.jpg',
    },
  },
  'workplace-mental-injury-how-do-i-know': {
    'content': `
      <h3>How do I know if I am experiencing work-related stress?</h3>
      <p>Work-related stress can affect how we feel, think and act. The following are some of the most common symptoms of work-related stress:</p>
      <ul>
        <li>Feeling tense, anxious or “on edge” at work</li>
        <li>Getting upset or angry easily</li>
        <li>Feeling sad, flat or not enjoying work</li>
        <li>Having trouble concentrating, making decisions or problem solving</li>
        <li>Feeling tired or less motivated than usual</li>
        <li>Panic-attack like symptoms, such as rapid heartbeat, chest pain or difficulties breathing</li>
        <li>Using more caffeine, alcohol or nicotine</li>
      </ul>
      <p>The way your work is designed or managed can add to stress, for example, having a high workload, long hours, or not being given clear instructions about what work is expected of you.</p>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration: person waving on a light red background',
      'height': '250px',
      'src': 'images/illustration-waving-person.svg',
    },
  },
  'retail-workplace-mental-injury-how-do-i-know': {
    'content': `
      <h3>How do I know if I am experiencing work-related stress?</h3>
      <p>Work-related stress can affect how we feel, think and act. The following are some of the most common symptoms of work-related stress:</p>
      <ul>
        <li>Feeling tense, anxious or “on edge” at work</li>
        <li>Getting upset or angry easily</li>
        <li>Feeling sad, flat or not enjoying work</li>
        <li>Having trouble concentrating, making decisions or problem solving</li>
        <li>Feeling tired or less motivated than usual</li>
        <li>Panic-attack like symptoms, such as rapid heartbeat, chest pain or difficulties breathing</li>
        <li>Using more caffeine, alcohol or nicotine</li>
      </ul>
      <p>The way your work is designed or managed can add to stress, for example, having a high workload, long hours, or not being given clear instructions about what work is expected of you.</p>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration: person waving on a light red background',
      'height': '250px',
      'src': 'images/illustration-waving-person-yellow.svg',
    },
  },
  'workplace-mental-injury-what-can-i-do': {
    'title': 'What can I do if I am experiencing work-related stress?',
    'headingLevel': 3,
    'content': `
    <p>Here are some things you can do at work to reduce your stress:</p>
    <ul>
      <li>Talk to your manager about how you are feeling. They might be able to reduce some of your workload or adjust your schedule. It is OK to say that something is beyond your capacity and you need more time or resources.</li>
      <li>You can also ask your manager to give you clear guidance on which of your work tasks are the highest priority. That way, you know you are focusing on the things that really need to get done.</li>
      <li>Make sure you are taking breaks during your work. Getting up, stretching or moving about can help you keep perspective. Getting away for a moment can make it easier to see clearly and identify when you might need support or a bigger break from the task.</li>
      <li>Connect with your HR support or with colleagues who are helpful, positive and resourceful. Discuss options and strategies for addressing your difficulties.</li>
      <li>Consider whether it’s time for a holiday.</li>
    </ul>
    <p>Here are some things you can do at home:</p>
    <ul>
      <li>Make healthy lifestyle changes: exercising more, eating healthier and getting enough sleep. Improving your physical health will improve your mental health too.</li>
      <li>Connect with family and friends who are helpful, positive and resourceful. Talk about how you might reduce the causes of stress at work.</li>
      <li>Take time to detach from work (including emails, texts and phone calls).</li>
      <li>Grow your skills and confidence in non-work-related activities and interests.</li>
      <li>Have fun and relax with your family, friends or on your own.</li>
    </ul>
    `,
  },
  'workplace-mental-injury-info': {
    'content': `
      <p><strong>Information and resources for managing stress</strong></p>
      <p>Learn different ways to cope with stress and solve problems <a href="https://thiswayup.org.au/coping-and-resilience-tools/" target="_blank" rel="external">thiswayup.org.au/coping-and-resilience-tools</a></p>
      <p>Encourage leaders to take steps towards creating a mentally healthy workplace <a href="https://www.workwell.vic.gov.au/" target="_blank" rel="external">www.workwell.vic.gov.au</a></p>
      <p>Encourage leaders or OHS representatives to learn more about work-related stress and how to prevent it <a href="//www.worksafe.vic.gov.au/work-related-stress" target="_blank" rel="external">www.worksafe.vic.gov.au/work-related-stress</a></p>
    `,
    'content2': `
      <p><strong>If you are distressed and need immediate emotional support, contact:</strong></p>
      <p>
        Lifeline: crisis support and suicide prevention service<br />
        <a href="tel:131114">13 11 14</a> | <a href="//www.lifeline.org.au" target="_blank" rel="external">www.lifeline.org.au</a>
      </p>
      <p>
        Beyond Blue: counselling and support service<br />
        <a href="tel:1300224636">1300 224 636</a> | <a href="//www.beyondblue.org.au" target="_blank" rel="external">www.beyondblue.org.au</a>
      </p>
    `,
    'hasBackgroundLight': true,
  },
  'workplace-mental-injury-contact': {
    'content': `
      <p><strong>The Health and Safety and Injury Compensation Advisory Service</strong> is available from 7:30am to 6:30pm, Monday to Friday.</p>
      <p>
        Call <a href="tel:1800136089">1800 136 089</a><br />
        Visit <a href="https://www.worksafe.vic.gov.au/speak-to-advisor" target="_blank" rel="external">worksafe.vic.gov.au/speak-to-advisor</a><br />
        To report an emergency at work 24 hours a day, 7 days a week, call <a href="tel:132360">13 23 60</a>
      </p>
    `,
    'content2': `
      <p><strong>The Victorian Equal Opportunity and Human Rights Commission (VEOHRC)</strong></p>
      <p>VEOHRC has a range of resources on anti-discrimination law and good employment practices.<br />
      <a href="tel:1300292153">1300 292 153</a> | <a href="//www.humanrightscommission.vic.gov.au" target="_blank" rel="external">www.humanrightscommission.vic.gov.au</a>
    `,
    'hasBackgroundLight': true,
  },
  'workplace-mental-injury-conflict-bullying': {
    'content': `
      <h3>What is workplace conflict and bullying?</h3>
      <p>Workplace conflict happens when people who work together disagree on what should be done, because they have competing needs, values or goals. A person can also be in conflict with themself, if they have different demands that they must choose between. While teams benefit from healthy debate and discussion, ongoing unresolved conflict can affect your physical and mental health.</p>
      <p>Unlike workplace conflict, workplace bullying is never healthy. Workplace bullying is repeated, unreasonable behaviour directed at an employee or group of employees that creates a risk to health and safety.</p>
    `,
    'img': {
      'align': 'right',
      'alt': 'Unhappy lady looking at her smartphone',
      'src': 'images/quitSocialMedia.jpg',
    },
  },
  'workplace-mental-injury-how-do-i-know-conflict': {
    'content': `
      <h3>How do I know if I am experiencing ongoing workplace conflict?</h3>
      <p>Workplace conflict can occur through misunderstandings, different perspectives, personality clashes, sharing or competing for tasks or resources, leadership and idea clashes to name a few.</p>
      <p>The signs and symptoms can be similar to work-related stress and may also include:</p>
      <ul>
        <li>Feeling less confident about interacting with a person or people safely</li>
        <li>Struggling to focus or concentrate when working with or near the person or people</li>
      </ul>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration: happy lady skipping and running forward on a light red background',
      'height': '250px',
      'src': 'images/illustration-lady-skipping.svg',
    },
  },
  'retail-workplace-mental-injury-how-do-i-know-conflict': {
    'content': `
      <h3>How do I know if I am experiencing ongoing workplace conflict?</h3>
      <p>Workplace conflict can occur through misunderstandings, different perspectives, personality clashes, sharing or competing for tasks or resources, leadership and idea clashes to name a few.</p>
      <p>The signs and symptoms can be similar to work-related stress and may also include:</p>
      <ul>
        <li>Feeling less confident about interacting with a person or people safely</li>
        <li>Struggling to focus or concentrate when working with or near the person or people</li>
      </ul>
    `,
    'img': {
      'align': 'left',
      'alt': 'Illustration: happy lady skipping and running forward on a light red background',
      'height': '250px',
      'src': 'images/illustration-lady-skipping-yellow.svg',
    },
  },
  'workplace-mental-injury-how-do-i-know-bullying': {
    'title': 'How do I know if I am experiencing workplace bullying?',
    'headingLevel': 3,
    'content': `
    <p>Bullying can happen in any workplace. It can involve one person or a group of people. Examples of bullying are:</p>
    <ul>
      <li>verbal abuse, like being sworn at, threatened, insulted, continual inappropriate and/or invalid criticism, name calling, belittling and humiliation, gossip and malicious rumours, or yelling</li>
      <li>abusive or offensive emails </li>
      <li>deliberately withholding information or resources that you need to do your work</li>
      <li>deliberately changing work rosters to inconvenience you</li>
      <li>interfering with your personal property or work equipment</li>
      <li>unreasonably excluding you from activities</li>
      <li>unreasonable demands, pressure and impossible deadlines targeting you or a group of employees specifically.</li>
    </ul>
    <p>The signs and symptoms can be similar to work-related stress and may also include:</p>
    <ul>
      <li>Feeling less confident about interacting with the person or people safely</li>
      <li>Struggling to focus or concentrate when working with or near the person or people</li>
      <li>Feeling worthless or stupid</li>
      <li>Not being able to stop thinking about the person, people or situation, especially outside of work hours.</li>
    </ul>
    `,
  },
  'workplace-mental-injury-not-bullying': {
    'title': 'What is not bullying?',
    'headingLevel': 3,
    'content': `
      It is important to keep in mind that reasonable management actions carried out in a reasonable way are not bullying. These include instructions on how to do tasks, setting attainable performance standards, providing constructive feedback, deciding not to promote someone and making reasonable changes to work tasks. 
    `,
  },
  'workplace-mental-injury-ongoing': {
    'title': 'What can I do about ongoing workplace conflict or bullying?',
    'headingLevel': 3,
    'content': `
    <p>Here are some things you might want to do, depending on what is happening and what your workplace is like:</p>
    <ul>
      <li>Talk to someone – your manager, a trusted friend, parent, teacher, family doctor, union representative, industry or apprenticeship scheme. Your Health &amp; Safety Representative (HSR) or HR officer can help you navigate your organisation’s systems on workplace bullying.</li>
      <li>If you are experiencing work-related conflict, consider talking to the person about how you can work together more peacefully.</li>
      <li>If you can, tell the person that what they are doing is unreasonable or inappropriate, that you are offended, and that you want it to stop. </li>
      <li>Check your workplace’s policies – ask about your organisation’s systems for preventing and responding to ongoing workplace conflict and bullying. </li>
      <li>Report workplace bullying using workplace procedures.</li>
    </ul>
    <p>If bullying cannot be resolved within your workplace, there are other places you can go for help:</p>
    `,
  },
  'workplace-mental-injury-info-conflict-bullying': {
    'content': `
      <p><strong>Information and resources about conflict in the workplace</strong></p>
      <ul>
        <li><a href="https://www.worksafe.vic.gov.au/work-related-stress-poor-workplace-relationships" target="_blank" rel="external">www.worksafe.vic.gov.au/work-related-stress-poor-workplace-relationships</a></li>
        <li><a href="https://www.relationshipsvictoria.com.au/resources/tip-sheets/relationships/managing-conflict-in-the-workplace/" target="_blank" rel="external">www.relationshipsvictoria.com.au/resources/tip-sheets/relationships/managing-conflict-in-the-workplace</a></li>
        <li><a href="https://www.betterhealth.vic.gov.au/health/HealthyLiving/workplace-conflict" target="_blank" rel="external">www.betterhealth.vic.gov.au/health/HealthyLiving/workplace-conflict</a></li>
      </ul>
    `,
    'content2': `
      <p><strong>Information and resources about bullying in the workplace</strong></p>
      <ul>
        <li><a href="//www.worksafe.vic.gov.au/introduction-workplace-bullying" target="_blank" rel="external" title="Read about workplace bullying on this WorkSafe VIC web page">www.worksafe.vic.gov.au/introduction-workplace-bullying</a></li>
        <li><a href="//www.safeworkaustralia.gov.au/doc/dealing-workplace-bullying-workers-guide" target="_blank" rel="external" title="Read the dealing with workplace bullying worker's guide on this SafeWork Australia web page">www.safeworkaustralia.gov.au/doc/dealing-workplace-bullying-workers-guide</a></li>
        <li><a href="//www.fairwork.gov.au/employee-entitlements/bullying-and-harassment" target="_blank" rel="external" title="Read about bullying and harassment on this fairwork.gov.au web page">www.fairwork.gov.au/employee-entitlements/bullying-and-harassment</a></li>
      </ul>
    `,
    'hasBackgroundLight': true,
  },
  'workplace-mental-injury-how-do-i-know-mental-injury': {
    'content': `
      <h3>How do I know if I have a mental injury?</h3>
      <p>The most accurate way is to speak with your medical health practitioner or psychologist. However, there are several online checklists that can help you identify if you are having symptoms:</p>
    `,
  },
  'workplace-mental-injury-checklist': {
    'content': `
      <p><strong>A general test for anxiety and depression</strong><br />
      <a href="//www.beyondblue.org.au/the-facts/anxiety-and-depression-checklist-k10" target="_blank" rel="external" title="Visit the general test on this Beyond Blue web page">www.beyondblue.org.au/the-facts/anxiety-and-depression-checklist-k10</a></p>
      <p><strong>Depression</strong><br />
      <a href="//www.blackdoginstitute.org.au/resources-support/digital-tools-apps/depression-self-test/" target="_blank" rel="external" title="Visit this self test on the Black Dog Institute web page">www.blackdoginstitute.org.au/resources-support/digital-tools-apps/depression-self-test/</a></p>
      <p><strong>Anxiety</strong><br />
      <a href="//www.beyondblue.org.au/the-facts/anxiety/anxiety-checklist" target="_blank" rel="external" title="Visit this self test on the Beyond blue web page">www.beyondblue.org.au/the-facts/anxiety/anxiety-checklist</a></p>
    `,
    'content2': `
      <p><strong>Self-harm or suicidal thoughts</strong><br />
      <a href="//www.beyondblue.org.au/the-facts/suicide-prevention/feeling-suicidal/suicidal-warning-signs" target="_blank" rel="external" title="Visit this self test on the Beyond Blue web page">www.beyondblue.org.au/the-facts/suicide-prevention/feeling-suicidal/suicidal-warning-signs</a></p>
      <p><strong>Post-Traumatic Stress Disorder (PTSD)</strong><br />
      <a href="//www.beyondblue.org.au/the-facts/anxiety/types-of-anxiety/ptsd" target="_blank" rel="external" title="Visit this self test on the Beyond Blue web page">www.beyondblue.org.au/the-facts/anxiety/types-of-anxiety/ptsd</a></p>
    `,
    'hasBackgroundLight': true,
  },
  'workplace-mental-injury-free-resources': {
    'content': `
      <h3>Free mental health resources</h3>
      <p>These publicly available websites can offer new ways of dealing with any thoughts, feelings and behaviours that are troubling you.</p>
    `,
  },
  'workplace-mental-injury-free-resources-list': {
    'content': `
      <p>
        <strong>My Compass</strong><br />
        A personalised self-help tool for your mental health<br />
        <a href="https://www.mycompass.org.au" target="_blank" rel="external" title="Go to the My Compass website">www.mycompass.org.au</a>
      </p>
      <p>
        <strong>HeadsUp</strong><br />
        Information and resources about mental health in the workplace<br />
        <a href="//www.headsup.org.au/your-mental-health" target="_blank" rel="external" title="Go to the HeadsUp website for resources about mental health in the workplace">www.headsup.org.au/your-mental-health</a> 
      </p>
    `,
    'content2': `
      <p>
        <strong>The Black Dog Institute</strong><br />
        Mental health resources and support tools that you can trust, because they are based on research and recommended by mental health professionals<br />
        <a href="//www.blackdoginstitute.org.au/resources-support" target="_blank" rel="external" title="Go to the Black Dog Institute's resources and support tools">www.blackdoginstitute.org.au/resources-support</a>
      </p>
    `,
    'hasBackgroundLight': true,
  },
  'workplace-mental-injury-your-right': {
    'title': 'Your right to mental safety at work',
    'headingLevel': 3,
    'content': `
      <p>Your employer has a responsibility to provide and maintain a working environment that is safe and without risks to your health, including your mental health. This includes providing and maintaining systems of work, information, instruction, training and supervision that enables employees to perform their work safely and without risks to physical or mental health.</p>
      <p>Health and Safety Representatives (HSRs) can raise issues on behalf of their Designated Work Group. They can also advise members of their DWG on how to approach a workplace issue, such as bullying. However, HSRs are not responsible for resolving the matter.</p>
    `,
  },
  'workplace-mental-injury-compensation': {
    'title': 'About Workers Compensation for mental injuries',
    'headingLevel': 3,
    'content': `
      <p>If you’re injured at work – including mental injuries – and need medical treatment or time off work, you may be eligible for compensation. This may include compensation for the time you had to take off work and for treatment expenses. Even if your Workers Compensation claim is not accepted, you may still be eligible for up to 13 weeks’ treatment to assist you with a workplace mental injury.</p>
      <p>You may also be able to get compensation if you’ve become permanently impaired, or if you find you need more than 52 weeks off work. You may also be eligible for superannuation benefits.</p>
    `,
  },
  'workplace-mental-injury-advisory': {
    'title': 'WorkSafe’s Health and Safety and Injury Compensation Advisory Service',
    'headingLevel': 3,
    'content': `
      <p>WorkSafe Victoria is responsible for promoting and enforcing health and safety in Victorian workplaces. It improves workplace health and safety, public safety, and delivers workers compensation services for Victorian employers and workers.</p>
    `,
  },
  'retail-landing-page-free-counselling': {
    'content': `
      <h3>What is the Wellbeing Support Service?</h3>
      <p>As an employer or manager, the Wellbeing Support Service allows you to provide access to 4 free telehealth appointments with a registered psychologist for employees in your workplace. You are eligible for the service if your workplace does not already offer free and confidential counselling, for example through an Employee Assistance Program (EAP).</p>
      <p>The Wellbeing Support Service aims to prevent mental health injuries in the workplace by giving your team a safe place to discuss and find solutions to any issues they’re having.</p>
      <div class="notification">
        <h4>Download your information pack</h4>
        <a href="images/manager-information-pack.pdf" class="button is-medium is-black has-text-weight-bold button-download-pack" download>
          <span class="icon"><img alt="Icon: download" src="images/icon-download.svg" /></span>
          <span>Download pack</span>
        </a>
      </div>
    `,
    'img': {
      'align': 'left',
      'alt': 'Man using laptop having an enojoyable video chat for work',
      'src': 'images/happy-worker-video-chat.jpg',
    },
  },
  'retail-free-counselling-assets-poster': {
    'content': `
      <h3>Posters and flyers</h3>
      <p>We suggest placing the provided posters in a visible, shared space in your workplace such as a breakroom.</p>
      <p>Provide printed flyers in the staff room and give them to managers who look after staff.</p>
      <p>Share information about this service through staff communication channels such as email and text.</p>
      <p>Share the digital poster to staff and networks online.</p>
      <div class="buttons">
        <a href="images/poster-wellbeing-support-service.pdf" class="button is-medium is-black has-text-weight-bold button-download-poster" download>
          <span class="icon"><img alt="Icon: download" src="images/icon-download.svg" /></span>
          <span>Download Poster</span>
        </a>
        <a href="images/poster-wellbeing-support-service-faqs.pdf" class="button is-medium is-black has-text-weight-bold button-download-flyer" download>
          <span class="icon"><img alt="Icon: download" src="images/icon-download.svg" /></span>
          <span>Download Flyer</span>
        </a>
      </div>
    `,
    'img': {
      'align': 'left',
      'alt': 'Wellbeing Support Service poster placed on the wall',
      'src': 'images/poster.jpg',
    },
  },
  'retail-free-counselling-assets-social-media': {
    'content': `
      <h3>Social media</h3>
      <p>Post in your digital community.</p>
      <p>Let your team and broader stakeholder network people know that support is available and welcome conversations about the service.</p>
      <p>The provided social tile can be used to share posts across social networking platforms.</p>
      <div class="notification">
        <h4>Share</h4>
        <div class="buttons">
          <a class="button is-medium is-black has-text-weight-bold button-share-linkedin" href="https://www.linkedin.com/shareArticle?url=https://www.wellbeingservice.com.au&title=Check-in%20with%20your%20mental%20health.%20Free%20and%20confidential%20support%20is%20available%20for%20all%20retail%20workers%20with%20WorkSafe's%20Wellbeing%20Support%20Service." target="_blank" rel="nofollow noopener">
            <span class="icon"><img alt="Icon: LinkedIn" src="images/icon-ln.svg" width="18" height="18" /></span>
            <span>LinkedIn</span>
          </a>
          <a class="button is-medium is-black has-text-weight-bold button-share-facebook" href="https://www.facebook.com/sharer.php?u=https://www.wellbeingservice.com.au" target="_blank" rel="nofollow noopener">
            <span class="icon"><img alt="Icon: Facebook" src="images/icon-fb.svg" width="24" height="24" /></span>
            <span>FaceBook</span>
          </a>
          <a class="button is-medium is-black has-text-weight-bold button-share-twitter" href="https://twitter.com/intent/tweet?url=https://www.wellbeingservice.com.au&text=Check-in%20with%20your%20mental%20health.%20Free%20and%20confidential%20support%20is%20available%20for%20all%20retail%20workers%20with%20WorkSafe's%20Wellbeing%20Support%20Service.&via=" target="_blank" rel="nofollow noopener">
            <span class="icon"><img alt="Icon: Twitter" src="images/icon-tw.svg" width="18" height="18" /></span>
            <span>Twitter</span>
          </a>
        </div>
      </div>
    `,
    'img': {
      'align': 'left',
      'alt': 'Wellbeing Support Service poster placed on the wall',
      'src': 'images/social-media.jpg',
    },
  },
  'retail-free-counselling-assets-influencers': {
    'content': `
      <h3>Share manually</h3>
      <ol>
        <li>Login to your LinkedIn and create a new post</li>
        <li>Copy and paste the message below into the post</li>
        <li>Replace <code>[LINK]</code> with the link provided by us</li>
        <li>If a preview image doesn't appear, please <a href="images/og.jpg" class="link-download-og-img-influencers" download title="Click to download the social media post image">download this image</a> and add it to your post</li>
      </ol>
      <h4>The message</h4>
      <blockquote>Many retail businesses are under significant strain. WorkSafe Victoria recognises this and is offering free and confidential support to retail workers in Victoria. As a leader in a retail business, the Wellbeing Support Service allows you to provide access to four free telehealth appointments for your team members with a registered psychologist. Learn how to access the Service by clicking below. <code>[LINK]</code></blockquote>
      <div class="buttons">
        <a href="images/og.jpg" class="button is-medium is-black has-text-weight-bold button-download-og-img-influencers" download>
          <span class="icon"><img alt="Icon: download" src="images/icon-download.svg" /></span>
          <span>Download image</span>
        </a>
      </div>
    `,
    'img': {
      'align': 'left',
      'alt': 'Wellbeing Support Service poster placed on the wall',
      'src': 'images/social-media.jpg',
    },
  },
};

export default contentBlocks;
