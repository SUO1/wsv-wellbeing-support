const navigation = {
  ece: {
    menu: [
      {
        'label': 'About the Wellbeing Service',
        'href': '/ece/about',
        'title': 'About the Wellbeing Service',
      },
      {
        'label': 'Where to get support',
        'href': '/ece/get-support',
        'title': 'Other places to get support with your mental health',
      },
      {
        'label': 'Workplace mental injury',
        'href': '/ece/workplace-mental-injury',
        'title': 'Information regarding your right to mental safety at work',
        'children': [
          {
            'label': 'Work-related stress',
            'href': '/ece/workplace-mental-injury/work-related-stress',
            'title': 'Work-related stress',
          },
          {
            'label': 'Workplace conflict and bullying',
            'href': '/ece/workplace-mental-injury/workplace-conflict-and-bullying',
            'title': 'Workplace conflict and bullying',
          },
        ],
      },
    ],
    root: '/ece',
  },
  retail: {
    menu: [
      {
        'label': 'About the Wellbeing Service',
        'href': '/retail/about',
        'title': 'About the Wellbeing Service',
      },
      {
        'label': 'Where to get support',
        'href': '/retail/get-support',
        'title': 'Other places to get support with your mental health',
      },
      {
        'label': 'Workplace mental injury',
        'href': '/retail/workplace-mental-injury',
        'title': 'Information regarding your right to mental safety at work',
        'children': [
          {
            'label': 'Work-related stress',
            'href': '/retail/workplace-mental-injury/work-related-stress',
            'title': 'Work-related stress',
          },
          {
            'label': 'Workplace conflict and bullying',
            'href': '/retail/workplace-mental-injury/workplace-conflict-and-bullying',
            'title': 'Workplace conflict and bullying',
          },
        ],
      },
    ],
    root: '/retail',
  },
};

export default navigation;
