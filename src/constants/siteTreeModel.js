import { config } from '../data';

const SITE_TREE_MODEL = {
  id: '',
  label: '', 
  route: '', 
  type: '', 
  parent: '', 
  skin: config.defaults.skin,
  isActive: false,
  isParent: false,
};

export default SITE_TREE_MODEL;
