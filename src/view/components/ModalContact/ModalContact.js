import state from '../../../state';
import setModalStatus from '../../../state/action/setModalStatus';

const Modal = {
  view () {
    return m(`.modal${state.isModalActive() ? '.is-active' : ''}`,
      m('.modal-background', { 'onclick': () => setModalStatus(false) }),
      m('.modal-card',
        m('.modal-card-body',
          m('iframe', {
            src: 'https://docs.google.com/forms/d/e/1FAIpQLSfwkcx1qcVoAGIKYrcru_vn1oGYOrzBo8IniFVnEUZHMreuaQ/viewform?embedded=true',
            width: '100%',
            height: '677',
            frameborder: '0',
            marginheight: '0',
            marginwidth: '0',
          }),
        ),
      ),
      m('button.modal-close.is-large', {
        'aria-label': 'close',
        onclick () {
          setModalStatus(false);
        },
      }),
    );
  },
};

export default Modal;
