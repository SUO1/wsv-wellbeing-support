import Button from '../Button';
import CallToActionButton from './CallToActionButton';
import CallToActionButtonDownload from './CallToActionButtonDownload';
import CallToActionButtonRequest from './CallToActionButtonRequest';

const getPrimaryButtonComponent = (type) => {
  switch (type) {
  case 'request':
    return CallToActionButtonRequest;
  case 'download':
    return CallToActionButtonDownload;
  default:
    return CallToActionButton;
  }
};

const CallToAction = {
  view: (v) => {
    const {
      title,
      tagline,
      primary = {},
      secondary = {},
      size = 'short',
    } = v.attrs;
    return m('.ws-call-to-action',
      m('.container',
        size === 'short' && title && m('.ws-call-to-action-title', title),
        size !== 'short' && tagline && m('.ws-call-to-action-content', m.trust(tagline)),
        m('.ws-call-to-action-buttons',
          m(getPrimaryButtonComponent(primary.type), { ...primary }),
          secondary.data &&
            m(Button,
              {
                appendSelector: '.is-ws-default.is-medium.is-last',
                href: `tel:${secondary.data}`,
                type: 'a',
              },
              secondary.label || secondary.data,
            ),
        ),
      ),
    );
  },
};

export default CallToAction;
