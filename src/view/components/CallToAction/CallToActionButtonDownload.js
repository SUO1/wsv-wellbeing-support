import Button from '../Button';

const CallToActionButtonDownload = {
  view (v) {
    const { label, data } = v.attrs;
    return m(
      Button,
      {
        appendSelector: '.is-black.is-medium.has-text-weight-bold.pl-4.pr-5.button-call-to-action-download',
        download: '',
        href: data,
        type: 'a',
      },
      m('span.icon',
        m('svg', { xmlns: 'http://www.w3.org/2000/svg', height: '24px', viewBox: '0 0 24 24', width: '24px', fill: '#ffffff' },
          m('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
          m('path', { d: 'M19 9h-4V3H9v6H5l7 7 7-7zM5 18v2h14v-2H5z' }),
        ),
      ),
      m('span', label || 'Download'),
    );
  },
};

export default CallToActionButtonDownload;
