import Button from '../Button';

const CallToActionButton = {
  view (v) {
    const { data, label } = v.attrs;
    return data && m(
      Button,
      {
        appendSelector: '.is-black.is-medium.has-text-weight-bold.px-5',
        href: `mailto:${data}`,
        type: 'a',
      },
      label || data,
    );
  },
};

export default CallToActionButton;
