const Button = {
  view: (v) => {
    const {
      appendSelector = '',
      download = false,
      href = '',
      onclick = v.onclick,
      type = 'button',
    } = v.attrs;
    return m(
      `${type}.button${appendSelector}`,
      {
        download,
        href,
        onclick,
      },
      v.children,
    );
  },
};

export default Button;
