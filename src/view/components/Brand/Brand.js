const Brand = {
  view: (v) => m(`.ws-brand${v.attrs.isInversed ? '.is-inversed' : ''}`,
    m('.ws-brand-text-1', 'Wellbeing'),
    m('.ws-brand-text-2', 'Support Service.'),
  ),
};

export default Brand;
