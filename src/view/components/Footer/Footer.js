import CallToAction from '../CallToAction';
import Logo from '../Logo';

const Footer = {
  view: (v) => {
    const {
      navData,
      pageData,
      configData,
    } = v.attrs;
    const footerNavData = navData.children;
    const callToActionData = {
      ...configData.callToAction,
      primary: pageData?.callToAction?.primary || configData.callToAction.primary,
    };
    return m('footer',
      m(CallToAction, { ...callToActionData, size: 'short' }),
      m('.footer.has-background-black',
        m('.container',
          m('.is-flex',
            m('a',
              { 
                'href': 'https://www.worksafe.vic.gov.au/',
                'target': '_blank',
              },
              m(Logo, { isInversed: true }),
            ),
            m('nav.is-flex.is-flex-grow-1.is-justify-content-flex-end.is-hidden-touch',
              m('ul.is-flex.is-align-items-center',
                footerNavData.map((node) => {
                  return m('li.ml-5',
                    m(m.route.Link,
                      { 
                        'class': 'has-text-white is-underlined', 
                        'href': node.route,
                      },
                      node.label,
                    ),
                  );
                }),
              ),
            ),
          ),
          m('hr.my-6'),
          m('aside.content.has-text-white',
            m('p',
              'WorkSafe Victoria acknowledges Aboriginal and Torres Strait Islander people as the Traditional Custodians of the land and acknowledges and pays respect to their Elders, past and present.'),
            m('p',
              'WorkSafe Victoria is committed to safe and inclusive work places, policies and services for people of LGBTIQ communities and their families.'),
          ),
          m('address.content.has-text-white', `WorkSafe Victoria © ${new Date().getFullYear()}`),
        ),
      ),
    );
  },
};

export default Footer;
