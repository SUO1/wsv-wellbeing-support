const InPageNav = {
  view: (v) => {
    const { pageData } = v.attrs;
    const { inPageNav } = pageData;
    const currentRoute = m.route.get();

    return m('section.has-background-light',
      m('.container',
        m('nav#inPageNav.tabs', { 'aria-label': 'Related pages'},
          m('ul',
            inPageNav.map((navItem) => {
              const { label, route } = navItem;
              return m(`li${route === currentRoute ? '.is-active.has-text-weight-bold' : ''}`, 
                m(m.route.Link, { href: route }, label),
              );
            }),
          ),
        ),
      ),
    );
  },
};

export default InPageNav;
