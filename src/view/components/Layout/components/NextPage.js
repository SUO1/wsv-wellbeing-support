import { pages } from '../../../../data';

const NextPage = {
  view: (v) => {
    const targetPageData = pages[v.attrs.nextPage];
    const { title, route } = targetPageData;

    return targetPageData && m('section.has-background-light',
      m('.container',
        m('aside.py-5.is-size-3.has-text-weight-bold.ws-doc-main',
          'Find out more about ',
          m(m.route.Link, { href: route}, `${title} →`),
        ),
      ),
    );
  },
};

export default NextPage;
