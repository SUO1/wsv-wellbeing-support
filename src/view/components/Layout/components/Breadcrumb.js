const Breadcrumb = {
  view: (v) => {
    const { navData } = v.attrs;
    const { label, route, children } = navData;

    const renderNode = (node) => {
      const { label, route, children } = node;
      return [
        m('li', m(m.route.Link, { href: route }, label)),
        children && renderChildren(children),
      ];
    };

    const renderChildren = (children) => {
      if (children) {
        children.map((node) => renderNode(node));
      }
    };

    return children && m('.container',
      m('.breadcrumb.ws-doc-main', { 'aria-label': 'breadcrumbs'},
        m('ul',
          m('li', m(m.route.Link, { href: route }, label)),
          children.map((node) => renderNode(node)),
        ),
      ),
    );
  },
};

export default Breadcrumb;
