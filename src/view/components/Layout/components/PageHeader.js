const PageHeader = {
  view: (v) => {
    return m('header',
      m('.ws-page-header',
        m('.container',
          m('.ws-doc-main',
            m('h1.ws-page-header-title', v.attrs.title),
          ),
        ),
      ),
    );
  },
};

export default PageHeader;
