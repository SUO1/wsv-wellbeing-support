import ContentBlock from '../ContentBlock';
import ModalContact from '../ModalContact';
import Navbar from '../Navbar';
import Footer from '../Footer';
import InPageNav from './components/InPageNav';
import NextPage from './components/NextPage';
import PageHeader from './components/PageHeader';
import { 
  config,
  contentBlocks as contentBlocksData,
} from '../../../data';
import state from '../../../state';

const Layout = {
  oninit: () => {
    state.action.resetPageToDefault();
    state.action.setActiveSiteTreeNodes(state.siteTree(), m.route.get());
  },
  onupdate: () => {
    state.action.resetPageToDefault();
  },
  view: (v) => {
    const { pages, pageID } = v.attrs;
    const pageData = pages[pageID];
    const {
      contentBlockIDs,
      inPageNav,
      nextPage,
      skin,
      subtitle,
      title,
      type,
    } = pageData;
    const configData = config[skin];
    const navData = state.siteTree().filter((node) => node.skin === skin)[0];

    return m(`.layout.has-skin-${skin || 'default'}`,
      m(Navbar, { navData, pageData, configData }),
      m('main#content',
        type === 'default' && m(PageHeader, { title }),
        inPageNav && m(InPageNav, { pageData }),
        contentBlockIDs.length > 0 && 
          m('article.pb-6',
            subtitle && m('.container', m('h2.ws-doc-main.is-size-4.mt-6', m.trust(subtitle))),
            contentBlockIDs.map((c, i) => m(ContentBlock, { contentData: { ...contentBlocksData[c], i } })),
          ),
        nextPage && m(NextPage, pageData),
      ),
      m(Footer, { navData, pageData, configData }),
      m(ModalContact),
    );
  },
};

export default Layout;
