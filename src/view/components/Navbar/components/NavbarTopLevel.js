import Brand from '../../Brand';
import Logo from '../../Logo';
import NavbarDefault from './NavbarDefault';
import CallToAction from '../../CallToAction';
import NavbarBurger from './NavbarBurger';
import NavbarMenu from './NavbarMenu';

const NavbarTopLevel = {
  view: (v) => {
    const { navData, pageData, configData } = v.attrs;
    const {
      callToAction,
      heroImage,
      title,
      type,
    } = pageData;
    const callToActionData = {
      ...configData.callToAction,
      size: 'long',
      primary: callToAction.primary || configData.callToAction.primary,
      tagline: callToAction.tagline || configData.callToAction.tagline,
    };
    console.log(pageData);
    console.log(callToActionData);
    const renderHeroImage = () => {
      const {
        focusX = '50%',
        focusY = '50%',
        src,
      } = heroImage;
      return m('.ws-navbar-top-level-image.is-flex-grow-1', { 
        style: src && `background-image: url('${src}');` + `background-position: ${focusX} ${focusY};`,
      });
    };
    return m('section.hero.ws-navbar-top-level',
      m('.hero-head',
        m('nav.navbar.is-hidden-desktop',
          m('.ws-doc-main',
            m('.navbar-brand.py-2.is-visible-desktop',
              m(Logo),
            ),
            type !== 'landing-page' && m('.ws-navbar-top-level-nav',
              m(NavbarBurger),
              m(NavbarMenu, { navData }),
            ),
          ),
        ),
      ),
      m('.hero-body.is-flex.is-align-items-stretch', 
        m('.container.is-flex.is-flex-direction-column.is-align-items-stretch',
          m('.mb-5.is-flex-tablet.is-flex-direction-row-reverse',
            m('.mb-3.is-hidden-touch',
              m(Logo),
            ),
            m('h1.is-flex-grow-1.ws-navbar-top-level-heading', m(Brand)),
          ),
          m('.ws-navbar-top-level-canvas.is-flex-grow-1.is-flex.is-flex-direction-column',
            renderHeroImage(),
            m('.ws-navbar-top-level-image-caption',
              m('h2.title.has-text-black.ws-navbar-top-level-text', title),
            ),
          ),
        ),
      ),
      callToActionData && m('footer.hero-foot',
        m('.container',
          m(CallToAction, { ...callToActionData }),
        ),
      ),
      type !== 'landing-page' && m(NavbarDefault, { navData }),
    );
  },
};

export default NavbarTopLevel;
