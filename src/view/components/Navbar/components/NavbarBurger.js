import state  from '../../../../state';

const NavbarBurger = {
  onupdate: () => {
    const { x, y } = state.targetPos();
    document.documentElement.scrollTo(x, y);
  },
  view: () => {
    const menuState = state.isMenuActive();
    return m(`button.navbar-burger.ws-navbar-burger${menuState ? '.is-active' : ''}`,
      {
        'aria-expanded': menuState,
        'aria-label': 'menu',
        'onclick': () => { 
          const d = document.documentElement;
          const pos = {
            x: d.scrollLeft,
            y: d.scrollTop,
          };
          state.targetPos(pos);
          state.isMenuActive(!menuState); 
        },
      },
      m(`span[aria-hidden]${menuState ? '.has-background-white' : ''}`),
      m(`span[aria-hidden]${menuState ? '.has-background-white' : ''}`),
      m(`span[aria-hidden]${menuState ? '.has-background-white' : ''}`),
    );
  },
};

export default NavbarBurger;
