import Brand from '../../Brand';
import NavbarBurger from './NavbarBurger';
import NavbarMenu from './NavbarMenu';
import Logo from '../../Logo';

const NavbarDefault = {
  view: (v) => {
    const { navData, pageData = {} } = v.attrs;
    const { skin } = pageData;
    
    document.addEventListener(
      'scroll',
      () => {
        const elContent = document.getElementById('content') || null;
        const elNav = document.getElementById('nav') || null;
        if (elContent && elNav) {
          const posShowMenu = elContent.offsetTop;
          const posCurrent = document.documentElement.scrollTop;
          if (posCurrent > posShowMenu) {
            elNav.classList.add('is-fixed-top');
          } else {
            elNav.classList.remove('is-fixed-top');
          }
        }
      },
    );

    return m('header',
      m('nav.navbar.ws-navbar', { 'aria-label': 'main navigation', 'id': 'nav' },
        m('.container',
          m('.navbar-start', 
            m(m.route.Link,
              {
                'class': 'ws-navbar-brand',
                'href': navData.route,
              },
              m(Logo, { isInversed: skin === 'ece' ? false : true }),
              m(Brand, { isInversed: skin === 'ece' ? false : true }),
            ),
          ),
          m('.navbar-end',
            m(NavbarMenu, { navData }),
          ),
          m(NavbarBurger),
        ),
      ),
    );
  },
};

export default NavbarDefault;
