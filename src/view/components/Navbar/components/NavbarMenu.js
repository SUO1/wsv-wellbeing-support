import NavbarBurger from './NavbarBurger';
import state from '../../../../state';

const NavbarMenu = {
  view: (v) => {
    const { navData } = v.attrs;
    const menuData = navData.children;
    return m(`.navbar-menu.ws-navbar-menu.is-flex-grow-0.ml-auto${(state.isMenuActive() ? '.is-active' : '')}`,
      m(NavbarBurger),
      menuData.map((node, i) => {
        const { 
          route, 
          label, 
          children,
          isActive,
        } = node;
        const hasChildren = Array.isArray(children) && children.length > 0;
        const NavbarItem = m(
          m.route.Link,
          {
            'class': `
              navbar-item ws-navbar-item has-text-weight-bold has-text-white 
              ${i === 0 ? ' is-first ' : ' '}
              ${isActive ? ' is-active ': ' '}
              ${hasChildren ? ' has-children ' : ' '}`,
            href: route,
          },
          label,
          hasChildren && m('.dropdown-trigger.ws-navbar-menu-toggle', { 'aria-hidden': true }),
        );

        return hasChildren
          ? m('.dropdown.ws-navbar-menu-dropdown',
            NavbarItem,
            m('.dropdown-menu', { role: 'menu' },
              m('.dropdown-content',
                children.map((node) => {
                  const { route, label, isActive } = node;
                  return m(m.route.Link, { 'class': `dropdown-item${isActive ? ' is-active' : ''}`, href: route }, label);
                }),
              ),
            ))
          : NavbarItem;
      }),
    );
  },
};

export default NavbarMenu;
