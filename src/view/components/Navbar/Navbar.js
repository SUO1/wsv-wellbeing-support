import NavbarDefault from './components/NavbarDefault';
import NavbarTopLevel from './components/NavbarTopLevel';

const Navbar = {
  view: (v) => {
    const { navData, pageData, configData } = v.attrs;
    const { type } = pageData;

    const Component = type === 'default'
      ? NavbarDefault
      : NavbarTopLevel;
    return m(Component, { navData, pageData, configData });
  },
};

export default Navbar;
