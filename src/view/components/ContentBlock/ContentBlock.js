const ContentBlock = {
  view: (v) => {
    const { contentData } = v.attrs;
    const {
      title,
      headingLevel,
      content,
      content2,
      img,
      hasBackgroundLight,
    } = contentData;

    const ColumnImage = (img) => {
      if (img) {
        const { align, alt, height, src } = img;
        return m('.column',
          { style: `${align === 'left' ? 'order: -1;' : ''}` },
          m('.ws-doc-main.has-text-centered',
            m('img', { 
              alt,
              src,
              style: height && `max-height: ${height};`,
            }),
          ),
        );
      }
    };

    return m('section.my-6.content-block',
      m(`.container.content${hasBackgroundLight ? '.has-background-highlight' : ''}`,
        title && m(`h${headingLevel}.mb-5.ws-doc-main`, title),
        m('.columns.is-tablet.is-variable.is-5',
          content && m('.column', m('.ws-doc-main', m.trust(content))),
          content2 && m('.column', m('.ws-doc-main', m.trust(content2))),
          ColumnImage(img),
        ),
      ),
    );
  },
};

export default ContentBlock;
