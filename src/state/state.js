import stream from 'mithril-stream';
import getSiteTree from './action/getSiteTree';
import { config, pages } from '../data';

const isMenuActive = stream(config.defaults.isMenuActive);
const isModalActive = stream(false);
const isNavbarFixed = stream(false);
const siteTree = stream(getSiteTree(pages));
const targetPos = stream({
  x: 0,
  y: 0,
});

export default {
  isMenuActive,
  isModalActive,
  isNavbarFixed,
  siteTree,
  targetPos,
};
