import state from './state';
import action from './action';

export default { ...state, action };
