import state from '../state';

const setModalStatus = isActive => state.isModalActive(isActive);

export default setModalStatus;
