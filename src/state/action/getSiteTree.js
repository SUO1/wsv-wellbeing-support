import { SITE_TREE_MODEL } from '../../constants';

const getChildrenSiteTreeNodes = (children, pages) => {
  if (Array.isArray(children) && children.length > 0) {
    return children.reduce(
      (a, pageID) => {
        a.push(getSiteTreeNode(pageID, pages));
        return a;
      },
      [],
    );
  }
};

const getSiteTreeNode = (pageID, pages) => {
  const pageData = pages[pageID];
  return Object.assign(
    { ...SITE_TREE_MODEL },
    { id: pageID },
    (({ label, route, type, parent, skin }) => ( 
      { label, route, type, parent, skin })
    )(pageData),
    { children: getChildrenSiteTreeNodes(pageData.children, pages) },
  );
};

const getSiteTree = (pages) => 
  Object.keys(pages).reduce(
    (a, pageID) => {
      const pageData = pages[pageID];
      if (!pageData.parent) {
        a.push(getSiteTreeNode(pageID, pages));
      }
      return a;
    },
    [],
  );

export default getSiteTree;
