import state from '../../state';

const setActiveRecursively = (siteTreeData, currentRoute) => {
  return siteTreeData.reduce((a, node) => {
    const isActive = currentRoute === node.route;
    const { children } = node;
    const data = {
      ...node,
      isActive,
      children: children && setActiveRecursively(children, currentRoute),
    };

    a.push(data);

    return a;
  }, []);
};

const setActiveSiteTreeNodes = (siteTreeData, currentRoute) => {
  state.siteTree(
    setActiveRecursively(siteTreeData, currentRoute),
  );
};

export default setActiveSiteTreeNodes;
