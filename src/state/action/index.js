import getSiteTree from './getSiteTree';
import resetPageToDefault from './resetPageToDefault';
import setActiveSiteTreeNodes from './setActiveSiteTreeNodes';
import setModalStatus from './setModalStatus';

export default {
  getSiteTree,
  resetPageToDefault,
  setActiveSiteTreeNodes,
  setModalStatus,
};
