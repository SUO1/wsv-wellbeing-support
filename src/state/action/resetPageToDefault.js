import { config } from '../../data';
import state from '../../state';

const resetPageToDefault = () => {
  state.isMenuActive(config.defaults.isMenuActive);
  state.isModalActive(false);
  window.scrollTo(0, 0);
};

export default resetPageToDefault;
