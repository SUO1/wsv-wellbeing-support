module.exports = {
  printWidth: 110,
  singleQuote: true,
  tabWidth: 2,
  arrowParens: 'always',
  semi: true,
  trailingComma: 'es5',
};
